package programacionweb.controladores;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import programacionweb.Modelos.modelo;
import programacionweb.Modelos.Atbash;

@WebServlet("/inicio")
public class ControladorInicio extends HttpServlet {
  public static String error1,error2,resultado;
  public ControladorInicio(){
    super();
  }
  @Override
  protected void doGet(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException{
        String texto,permutacion;
        if(solicitud.getParameter("OpCifrado")!=null){
          texto=solicitud.getParameter("EntradaTexto");
          permutacion=solicitud.getParameter("Permutacion");
          if(modelo.verificaPermutacion(permutacion)){
          if(solicitud.getParameter("OpCifrado").equalsIgnoreCase("Cifrar"))
          {
            resultado=modelo.cifrado(texto, permutacion);
          }
          else{
            if (modelo.tdesi(texto, permutacion))
            resultado=modelo.descifrado(texto, permutacion);
          }
          solicitud.setAttribute("resultado", resultado);
        }else{
          resultado="La permutacion es invalida ingrese una permutacion valida.";
          solicitud.setAttribute("resultado", resultado);
        }
      }
         var contextoServlet = solicitud.getServletContext();
         var despachadorSolicitud =
            contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
            despachadorSolicitud.forward(solicitud, respuesta);

  }
  @Override
  protected void doPost(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException{
        String texto,permutacion,Alfabetoclave;
        if(solicitud.getParameter("accion")!=null){
        if(solicitud.getParameter("OpCifrado")!=null){
          texto=solicitud.getParameter("EntradaTexto");
          permutacion=solicitud.getParameter("Permutacion");
          if (solicitud.getParameter("accion").equalsIgnoreCase("Aceptar")){
          if(modelo.verificaPermutacion(permutacion)){
          if(solicitud.getParameter("OpCifrado").equalsIgnoreCase("Cifrar"))
          {
            resultado=modelo.cifrado(texto, permutacion);
          }
          else{
            if (modelo.tdesi(texto, permutacion))
            resultado=modelo.descifrado(texto, permutacion);
          }
          solicitud.setAttribute("resultado", resultado);
        }else{
          resultado="La permutacion es invalida ingrese una permutacion valida.";
          solicitud.setAttribute("resultado", resultado);
        }
        } else if(solicitud.getParameter("accion").equalsIgnoreCase("Atbash")){
          rep2(solicitud, respuesta);
      }else if(solicitud.getParameter("accion").equalsIgnoreCase("Grupo")){
        rep(solicitud, respuesta);
      }else if(solicitud.getParameter("accion").equalsIgnoreCase("AceptarAtbash")){
                  Alfabetoclave=solicitud.getParameter("Alfabetoclave");
        if(Atbash.verfifica(Alfabetoclave)){
          if(solicitud.getParameter("OpCifrado").equalsIgnoreCase("Cifrar"))
          {
            resultado=Atbash.codificar(texto, Alfabetoclave);
          }
          else{
            resultado=Atbash.codificar(texto, Alfabetoclave);
          }
          solicitud.setAttribute("resultado", resultado);
        }else{
          resultado="La permutacion es invalida ingrese una permutacion valida.";
          solicitud.setAttribute("resultado", resultado);
        }
      }
    }
    }
        rep(solicitud, respuesta);
  }
  protected void rep(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException
      {
        var contextoServlet = solicitud.getServletContext();
        var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
        despachadorSolicitud.forward(solicitud, respuesta);
      }
      protected void processRequest(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
      }
      protected void rep2(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException
      {
        var contextoServlet = solicitud.getServletContext();
        var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/Vista2.jsp");
        despachadorSolicitud.forward(solicitud, respuesta);
      }

}
